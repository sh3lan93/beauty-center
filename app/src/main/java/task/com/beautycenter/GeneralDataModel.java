package task.com.beautycenter;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by shalan on 4/7/18.
 */

public class GeneralDataModel {

    @SerializedName("data")
    private List<DataModel> dataModelList;

    public GeneralDataModel() {
    }

    public GeneralDataModel(List<DataModel> dataModelList) {
        this.dataModelList = dataModelList;
    }

    public List<DataModel> getDataModelList() {
        return dataModelList;
    }

    public void setDataModelList(List<DataModel> dataModelList) {
        this.dataModelList = dataModelList;
    }
}
