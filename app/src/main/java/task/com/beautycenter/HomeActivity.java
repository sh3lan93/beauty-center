package task.com.beautycenter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

public class HomeActivity extends AppCompatActivity implements LoginFragment.OnFragmentInteractionListener, HomeFragment.OnFragmentInteractionListener, DetailsFragment.OnFragmentInteractionListener{

    private LoginFragment loginFragment;
    private int containerID = R.id.container;
    private HomeFragment homeFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        applyFullScreenMode();
        setContentView(R.layout.activity_home);
        if (!PrefManager.newInstance(this).getUserHasLogged())
            openLoginFragment();
        else
            openHomeFragment();
    }

    private void applyFullScreenMode() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private void openLoginFragment(){
        loginFragment = LoginFragment.newInstance();
        getSupportFragmentManager().beginTransaction().add(containerID, loginFragment).commit();
    }

    @Override
    public void onLoginBtnClicked() {
        PrefManager.newInstance(this).setUserHasLogged(true);
        openHomeFragment();
    }

    private void openHomeFragment(){
        homeFragment = HomeFragment.newInstance();
        getSupportFragmentManager().beginTransaction().add(containerID, homeFragment).commit();
    }

    @Override
    public void onSeeMoreClicked(int position) {
        DetailsFragment detailsFragment = DetailsFragment.newInstance(position);
        getSupportFragmentManager().beginTransaction().replace(containerID, detailsFragment).addToBackStack(null).commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
