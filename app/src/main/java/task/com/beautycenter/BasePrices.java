package task.com.beautycenter;

import com.google.gson.annotations.SerializedName;

/**
 * Created by shalan on 4/8/18.
 */

public class BasePrices {
    @SerializedName("price")
    double price;

    public BasePrices() {
    }

    public BasePrices(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
