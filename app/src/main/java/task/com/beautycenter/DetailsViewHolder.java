package task.com.beautycenter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by shalan on 4/8/18.
 */

public class DetailsViewHolder extends RecyclerView.ViewHolder {
    TextView firstRaw;
    TextView secondView;
    ImageView imageView;


    public DetailsViewHolder(View itemView) {
        super(itemView);
        firstRaw = itemView.findViewById(R.id.firstRaw);
        secondView = itemView.findViewById(R.id.secondRaw);
        imageView = itemView.findViewById(R.id.image);
    }

    public TextView getFirstRaw() {
        return firstRaw;
    }

    public TextView getSecondView() {
        return secondView;
    }

    public ImageView getImageView() {
        return imageView;
    }
}
