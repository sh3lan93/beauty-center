package task.com.beautycenter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by shalan on 4/8/18.
 */

public class DetailsAdapter extends RecyclerView.Adapter<DetailsViewHolder> {

    public static final int BOOKS_TYPE = 1;
    public static final int COFFEE_TYPE = 2;
    public static final int PRICES_TYPE = 3;
    public static final int PICTURES_TYPE = 4;

    private Context context;
    private List<MainDataModel> models;
    private List<Integer> images;

    private int type;
    public DetailsAdapter(Context context, List<MainDataModel> models, int type) {
        this.context = context;
        this.models = models;
        this.type = type;
    }

    public DetailsAdapter(Context context, List<Integer> images) {
        this.context = context;
        this.images = images;
        this.type = PICTURES_TYPE;
    }

    @Override
    public DetailsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.details_item_view, parent, false);
        DetailsViewHolder holder = new DetailsViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(DetailsViewHolder holder, int position) {


        if (type == BOOKS_TYPE){
            MainDataModel model = models.get(position);
            holder.getFirstRaw().setText("");
            holder.getSecondView().setText("");
            holder.getFirstRaw().setText(model.getName());
            holder.getSecondView().setText(model.getDes());
        }
        else if (type == COFFEE_TYPE){
            MainDataModel model = models.get(position);
            holder.getFirstRaw().setText("");
            holder.getSecondView().setText("");
            holder.getFirstRaw().setText(model.getName());
            holder.getSecondView().setVisibility(View.GONE);
        }
        else if (type == PRICES_TYPE){
            MainDataModel model = models.get(position);
            holder.getFirstRaw().setText("");
            holder.getSecondView().setText("");
            holder.getFirstRaw().setText(model.getName());
            for (BasePrices prices : model.getBasePricesList()){
                holder.getSecondView().setText(holder.getSecondView().getText().toString() + "\n-\t" + prices.getPrice() + "\t" + context.getString(R.string.currency));
            }
        }
        else if (type == PICTURES_TYPE){
            holder.getFirstRaw().setVisibility(View.GONE);
            holder.getSecondView().setVisibility(View.GONE);
            holder.getImageView().setVisibility(View.VISIBLE);
            holder.imageView.setImageResource(images.get(position));
        }
    }

    @Override
    public int getItemCount() {
        if (type != PICTURES_TYPE)
            return models.size();
        else
            return images.size();
    }
}
