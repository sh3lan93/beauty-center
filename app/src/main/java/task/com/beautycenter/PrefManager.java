package task.com.beautycenter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by shalan on 4/7/18.
 */

public class PrefManager {

    private static final String SHARED_PREF_FILE_NAME = "BEAUTY";
    private static Context context;
    private static PrefManager prefManager;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private static final String USER_LOGIN = "user_login";


    @SuppressLint("CommitPrefEdits")
    public PrefManager(Context context) {
        sharedPreferences = context.getSharedPreferences(SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public static PrefManager newInstance(Context context) {
        prefManager = new PrefManager(context);
        return prefManager;
    }

    public void setUserHasLogged(boolean hasLogged){
        editor.putBoolean(USER_LOGIN, hasLogged).commit();
    }

    public boolean getUserHasLogged(){
        return sharedPreferences.getBoolean(USER_LOGIN, false);
    }
}
