package task.com.beautycenter;

import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toolbar;

import java.util.List;


public class HomeFragment extends Fragment implements HomePagerAdapter.SeeMoreListener {
    private OnFragmentInteractionListener mListener;
    private ViewPager pager;

    private HomePagerAdapter adapter;
    private List<DataModel> dataModelList;
    private JsonManager manager;

    private android.support.v7.widget.Toolbar toolbar;

    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        pager = view.findViewById(R.id.pager);
        manager = new JsonManager(getContext(), JsonManager.DATA_TYPE);
        manager.readJsonFile();
        dataModelList = manager.getData();
        adapter = new HomePagerAdapter(getContext(), dataModelList, this);
        pager.setAdapter(adapter);
        toolbar = view.findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.app_name));
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        return view;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.home_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.contactUs:
                showDialog(R.layout.contact_us);
                break;
            case R.id.question:
                showDialog(R.layout.question);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showDialog(int view){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setView(view);
        builder.setPositiveButton(R.string.submit, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.create().show();
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onSeeMoreClicked(int position) {
        mListener.onSeeMoreClicked(position);
    }


    public interface OnFragmentInteractionListener {
        void onSeeMoreClicked(int position);
    }
}
