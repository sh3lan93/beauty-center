package task.com.beautycenter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.flaviofaria.kenburnsview.KenBurnsView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shalan on 4/7/18.
 */

public class HomePagerAdapter extends PagerAdapter {

    private Context context;
    private List<DataModel> dataModelList;
    private List<Integer> imagesIDList = new ArrayList<>();

    private SeeMoreListener listener;
    public HomePagerAdapter(Context context, List<DataModel> dataModelList, SeeMoreListener listener) {
        this.context = context;
        this.dataModelList = dataModelList;
        this.listener = listener;
        prepareData();
    }

    private void prepareData() {
        for (DataModel dataModel : dataModelList){
            switch (dataModel.getId()){
                case 1:
                    imagesIDList.add(R.drawable._1);
                    break;
                case 2:
                    imagesIDList.add(R.drawable._2);
                    break;
                case 3:
                    imagesIDList.add(R.drawable._3);
                    break;
                case 4:
                    imagesIDList.add(R.drawable._4);
                    break;
                case 5:
                    imagesIDList.add(R.drawable._5);
                    break;
                case 6:
                    imagesIDList.add(R.drawable._6);
                    break;
                case 7:
                    imagesIDList.add(R.drawable._7);
                    break;
                case 8:
                    imagesIDList.add(R.drawable._8);
                    break;
                case 9:
                    imagesIDList.add(R.drawable._9);
                    break;
                case 10:
                    imagesIDList.add(R.drawable._10);
                    break;
                case 11:
                    imagesIDList.add(R.drawable._11);
                    break;
            }
        }
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.home_data_item_view, container, false);
        initializeViews(view, position);
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    private void initializeViews(View view, final int position) {
        KenBurnsView kenBurnsView = view.findViewById(R.id.dataImage);
        kenBurnsView.setImageResource(imagesIDList.get(position));

        TextView title = view.findViewById(R.id.title);
        title.setText(dataModelList.get(position).getTitle());

        TextView description = view.findViewById(R.id.description);
        description.setText(dataModelList.get(position).getDescription());

        Button button = view.findViewById(R.id.seeMore);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onSeeMoreClicked(position);
            }
        });
    }

    @Override
    public int getCount() {
        return dataModelList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public interface SeeMoreListener{
        void onSeeMoreClicked(int position);
    }
}
