package task.com.beautycenter;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


public class DetailsFragment extends Fragment {

    private static final String ARG_POSITION = "position";
    private OnFragmentInteractionListener mListener;
    private int position;

    private RecyclerView recyclerView;

    private JsonManager jsonManager;
    private BaseDataModel detailsData;

    private List<MainDataModel> dataModels;
    private DetailsAdapter adapter;

    private Toolbar toolbar;

    private List<Integer> images = new ArrayList<>();
    public DetailsFragment() {
        // Required empty public constructor
    }


    public static DetailsFragment newInstance(int position) {
        DetailsFragment fragment = new DetailsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_POSITION, position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            position = getArguments().getInt(ARG_POSITION);
            switch (position){
                case 0:
                    jsonManager = new JsonManager(getContext(), JsonManager.PRICES_TYPE);
                    break;
                case 1:
                    jsonManager = new JsonManager(getContext(), JsonManager.COFFEE_TYPE);
                    break;
                case 2:
                    images.add(R.drawable._1);
                    images.add(R.drawable._2);
                    images.add(R.drawable._3);
                    images.add(R.drawable._4);
                    images.add(R.drawable._5);
                    images.add(R.drawable._6);
                    images.add(R.drawable._7);
                    images.add(R.drawable._8);
                    images.add(R.drawable._9);
                    images.add(R.drawable._10);
                    images.add(R.drawable._11);
                    break;
                case 3:
                    jsonManager = new JsonManager(getContext(), JsonManager.BOOKS_TYPE);
                    break;
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_details, container, false);
        recyclerView = view.findViewById(R.id.detailsRecycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        if (jsonManager != null){
            jsonManager.readJsonFile();
            this.detailsData = jsonManager.getDetailsData();
            this.dataModels = detailsData.getMainDataModels();
        }
        switch (position){
            case 0:
                adapter = new DetailsAdapter(getContext(), dataModels, DetailsAdapter.PRICES_TYPE);
                break;
            case 1:
                adapter = new DetailsAdapter(getContext(), dataModels, DetailsAdapter.COFFEE_TYPE);
                break;
            case 2:
                adapter = new DetailsAdapter(getContext(), images);
                break;
            case 3:
                adapter = new DetailsAdapter(getContext(), dataModels, DetailsAdapter.BOOKS_TYPE);
                break;
        }
        recyclerView.setAdapter(adapter);
        toolbar = view.findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.app_name));
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_keyboard_backspace_white_24dp);
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {

    }
}
