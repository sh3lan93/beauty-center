package task.com.beautycenter;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by shalan on 4/8/18.
 */

public class BaseDataModel {

    @SerializedName("data")
    List<MainDataModel> mainDataModels;

    public BaseDataModel() {
    }

    public List<MainDataModel> getMainDataModels() {
        return mainDataModels;
    }

    public void setMainDataModels(List<MainDataModel> mainDataModels) {
        this.mainDataModels = mainDataModels;
    }

    public BaseDataModel(List<MainDataModel> mainDataModels) {
        this.mainDataModels = mainDataModels;
    }
}
