package task.com.beautycenter;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by shalan on 4/8/18.
 */

public class MainDataModel {
    @SerializedName("id")
    int id;

    @SerializedName("name")
    String name;

    @SerializedName("des")
    String des;

    @SerializedName("price")
    List<BasePrices> basePricesList;

    public MainDataModel() {
    }

    public MainDataModel(int id, String name, String des, List<BasePrices> basePricesList) {
        this.id = id;
        this.name = name;
        this.des = des;
        this.basePricesList = basePricesList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public List<BasePrices> getBasePricesList() {
        return basePricesList;
    }

    public void setBasePricesList(List<BasePrices> basePricesList) {
        this.basePricesList = basePricesList;
    }
}
