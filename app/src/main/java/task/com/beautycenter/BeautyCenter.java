package task.com.beautycenter;

import android.app.Application;
import android.content.res.Configuration;

import java.util.Locale;

/**
 * Created by shalan on 4/8/18.
 */

public class BeautyCenter extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        setArabicMode();
    }

    private void setArabicMode(){
        Locale locale = new Locale("ar");
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        getApplicationContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }
}
