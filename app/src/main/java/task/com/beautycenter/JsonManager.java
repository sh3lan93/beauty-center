package task.com.beautycenter;

import android.content.Context;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by shalan on 4/7/18.
 */

public class JsonManager {

    public static final int DATA_TYPE = 1;
    public static final int BOOKS_TYPE = 2;
    public static final int COFFEE_TYPE = 3;
    public static final int PRICES_TYPE = 4;

    private static final String DATA_JSON_FILE_NAME = "data.json";
    private static final String BOOKS_JSON_FILE_NAME = "books.json";
    private static final String COFFEE_JSON_FILE_NAME = "coffe.json";
    private static final String PRICES_JSON_FILE_NAME = "prices.json";


    private static final String JSON_FORMAT = "UTF-8";
    private static java.lang.String FINAL_JSON_FILE_NAME = null;
    private final Context context;
    private String jsonContent;

    public JsonManager(Context context, int type){
        this.context = context;
        switch (type){
            case DATA_TYPE:
                FINAL_JSON_FILE_NAME = DATA_JSON_FILE_NAME;
                break;
            case BOOKS_TYPE:
                FINAL_JSON_FILE_NAME = BOOKS_JSON_FILE_NAME;
                break;
                case COFFEE_TYPE:
                    FINAL_JSON_FILE_NAME = COFFEE_JSON_FILE_NAME;
                    break;
            case PRICES_TYPE:
                FINAL_JSON_FILE_NAME = PRICES_JSON_FILE_NAME;
                break;
        }
    }

    public void readJsonFile(){
        InputStream inputStream = null;
        try {
            inputStream = context.getAssets().open(FINAL_JSON_FILE_NAME);
            int size = inputStream.available();
            byte [] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            this.jsonContent = new String(buffer, JSON_FORMAT);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<DataModel> getData(){
        Gson gson = new Gson();
        GeneralDataModel generalDataModel = gson.fromJson(this.jsonContent, GeneralDataModel.class);
        return generalDataModel.getDataModelList();
    }

    public BaseDataModel getDetailsData(){
        Gson gson = new Gson();
        BaseDataModel model = gson.fromJson(this.jsonContent, BaseDataModel.class);
        return model;
    }
}
